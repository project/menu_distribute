<?php

/**
 * @file
 * Create module admin forms.
 */

/**
 * Admin form for selecting site menus available for export.
 */
function menu_distribute_admin_sources($form, &$form_state) {
  $menu_list = menu_get_menus();
  $menu_list_paths = _menu_distribute_get_export_paths($menu_list);

  $form['menu_distribute_exposed'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Exposed Menus'),
    '#description' => t('Select the menus to be available for distribution.'),
    '#options' => $menu_list,
    '#options_descriptions' => $menu_list_paths,
    '#default_value' => variable_get('menu_distribute_exposed', array()),
    '#after_build' => array('_menu_distribute_checkbox_descriptions'),
  );

  return system_settings_form($form);
}

/**
 * Admin form for setting up menu mappings.
 */
function menu_distribute_admin_mappings($form, &$form_state) {
  $menu_mappings = variable_get('menu_distribute_mappings', array());

  foreach ($menu_mappings as $delta => $mapping) {
    $form = _menu_distribute_create_mapping_group($form, $delta, $mapping);
  }

  $form = _menu_distribute_create_mapping_group($form, count($menu_mappings));

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Submit callback for the mappings form
 */
function menu_distribute_admin_mappings_submit($form, &$form_state) {
  $submitted_data = $form_state['values'];
  // Count the number of mappings we're creating.
  $mapping_count = count($form_state['groups']) - 1;
  $new_mappings = array();

  // Loop over mappings and prepare them for save.
  foreach(range(0, $mapping_count) as $delta) {
    if (
      !empty($submitted_data['source_url_' . $delta]) &&
      !empty($submitted_data['menu_name_' . $delta])
    ) {
      $new_mappings[] = array(
        'source_url' => $submitted_data['source_url_' . $delta],
        'menu_name' => $submitted_data['menu_name_' . $delta],
      );
    }
  }

  // Override the list of mappings.
  variable_set('menu_distribute_mappings', $new_mappings);

  drupal_set_message(t('Updated mapping configuration.'), 'status');
}

/**
 * Helper function to create new mapping group fieldsets.
 */
function _menu_distribute_create_mapping_group($form, $delta, $mapping = array()) {
  $menu_list = menu_get_menus();

  $form['mapping_' . $delta] = array(
    '#type' => 'fieldset',
    '#title' => t('Mapping !count', array('!count' => ($delta + 1))),
    '#collapsible' => FALSE,
  );

  $form['mapping_' . $delta]['source_url_' . $delta] = array(
    '#type' => 'textfield',
    '#title' => t('Source URL'),
    '#default_value' => isset($mapping['source_url']) ? $mapping['source_url'] : '',
  );

  $form['mapping_' . $delta]['menu_name_' . $delta] = array(
    '#type' => 'select',
    '#title' => t('Destination Menu'),
    '#options' => $menu_list,
    '#default_value' => isset($mapping['menu_name']) ? $mapping['menu_name'] : NULL,
    '#empty_option' => t('- Choose Menu -'),
  );

  return $form;
}

/**
 * Returns an array of menu export paths keyed by menu ID.
 */
function _menu_distribute_get_export_paths($menus) {
  global $base_url;
  global $base_path;

  $paths = array();
  foreach ($menus as $machine_name => $human_name) {
    $paths[$machine_name] = $base_url . $base_path . 'menu-distribute/' . $machine_name;
  }
  return $paths;
}

/**
 * Provide a description to each checkbox with the URL to access the export.
 */
function _menu_distribute_checkbox_descriptions($element, &$form_state) {
  foreach (element_children($element) as $key) {
    $element[$key]['#description'] = $element['#options_descriptions'][$key];
  }
  return $element;
}
