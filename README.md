# Menu Distribute

The Menu Distribute module leverages [Menu Import](https://www.drupal.org/project/menu_import) to allow for automated syncing of Drupal menu items on cron run.

# Installation

## Exposing Menus

Visit the Overview configuration page (admin/config/services/menu-distribute). Select which site menus you wish to expose for other Drupal sites. Below each menu name is the URL that will be used in creating mappings. Only selected menus will get the URL created.

## Creating Mappings

To allow another website (or the same site) to import your exposed menu configuration, visit the Mapping config page (admin/config/services/menu-distribute/mappings). Paste the URL recieved from the site exposing the menu config, and select which menu should sync with those settings. Click "Save". Create additional mappings as needed.
